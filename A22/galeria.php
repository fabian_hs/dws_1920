<?php
require 'utils/utils.php';
require 'exceptions/FileException.php';
require 'utils/File.php';
require 'entity/ImagenGaleria.php';


$descripcion = "";
$mensaje = "";


if ($_SERVER["REQUEST_METHOD"]==="POST") {

    try {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
        $imagen = new File("imagen", $tiposAceptados);

        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY);
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY,ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);
        $mensaje = "Datos enviados";

    } catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    }
  }

require "view/galeria.view.php";
?>
