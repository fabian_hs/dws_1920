<?php
$error = array();
$nombre = htmlspecialchars($_POST['nombre'] ?? null);
$apellido = htmlspecialchars($_POST['apellido'] ?? null);
$email= htmlspecialchars($_POST['email'] ?? null);
$tema = htmlspecialchars($_POST['tema'] ?? null);
$mensaje = htmlspecialchars($_POST['mesaje'] ?? null);

if ($nombre == null){
  array_push($error, "El campo nombre es obligatorio");
}

if ($email == null){
  array_push($error, "El campo email es obligatorio");
}else{
  // 4. El e-mail debe contener un correo electrónico válido
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    array_push($error,"Esta dirección de correo ($email) no es válida.");
  }
}

if ($tema == null){
  array_push($error, "El campo tema es obligatorio");
}
  require 'utils/utils.php';
  require "view/contact.view.php";
?>
