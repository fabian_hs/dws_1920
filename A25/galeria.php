<?php
require 'utils/utils.php';
require 'utils/File.php';

require 'exceptions/FileException.php';
require 'exceptions/QueryException.php';
require 'exceptions/AppException.php';

require 'entity/ImagenGaleria.php';
require 'entity/Categoria.php';

require 'core/App.php';

require 'database/Connection.php';
require 'database/QueryBuilder.php';

require 'repository/ImagenGaleriaRepository.php';
require 'repository/CategoriaRepository.php';

$descripcion = "";
$mensaje = "";



  try {

    $config = require_once("app/config.php");
    //Guardamos la configuración en el contenedor
    App::bind("config", $config);
    //$connection = App::getConnection();

    //$queryBuilder = new QueryBuilder("imagenes","ImagenGaleria");
    $imagenGaleriaRepository = new ImagenGaleriaRepository();
    $categoriaRepository = new CategoriaRepository();

      if ($_SERVER["REQUEST_METHOD"]==="POST") {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        $categoria = trim(htmlspecialchars($_POST["categoria"]));

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
        $imagen = new File("imagen", $tiposAceptados);

        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY);
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY,ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

        $imagenGaleria = new ImagenGaleria($imagen->getFileName(),$descripcion, $categoria);
      //  $queryBuilder->save($imagenGaleria);
          $imagenGaleriaRepository->save($imagenGaleria);
        $mensaje = "se ha guardado la imagen en la BBDD.";

        /*$insetar = "INSERT INTO fotografia.imagenes (nombre,descripcion) VALUES (:nombre, :descripcion)";
        //$sentencias = $connection->prepare($insetar);
        //$sentencias -> bindParam(':descripcion',$descripcion);
        $mensaje = "Datos enviados";
        $nombre = $imagen->getFileName();
        $sentencias -> bindParam(':nombre',$nombre);
        $sentencias->execute();*/
        }
        /*$exception = new QueryBuilder('imagenes','ImagenGaleria');
        $arrayImagen = $exception->findAll();*/
        //$imagenes = $queryBuilder->findAll();
        $arrayImagen = $imagenGaleriaRepository->findAll();
        $categorias = $categoriaRepository->findAll();


    } catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    }
    catch (QueryException $queryException) {

        $errores [] = $queryException->getMessage();

    }
    catch (AppException $appException) {

      throw new AppException("No se ha podido conectar con la BBDD");

    }




require "view/galeria.view.php";
?>
