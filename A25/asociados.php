<?php
require 'utils/utils.php'; // Para el nav
require 'exceptions/FileException.php';
require 'utils/File.php';
require 'entity/Asociado.php';

$nombre = "";
$descripcion="";


if ($_SERVER["REQUEST_METHOD"]==="POST") {

  try {
    $nombre = trim(htmlspecialchars($_POST["nombre"] ?? null));
    if($nombre != null){
      $error = null;
      $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
      $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
      $imagen = new File("imagen", $tiposAceptados);

      $imagen->saveUploadFile(Asociado::RUTA_IMAGENES_ASOCIADOS);
      $mensaje = "Datos enviados";
    }else{
      $error = "Falta nombre";
    }
  }
  catch (FileException $fileException) {

      $errores [] = $fileException->getMessage();

  }
}

  require "view/asociados.view.php";
 ?>
