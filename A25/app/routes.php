<?php

return [

 "" => "app/controllers/index.php",

 "about" => "app/controllers/about.php",

 "asociado" => "app/controllers/asociados.php",

 "blog" => "app/controllers/blog.php",

 "contact" => "app/controllers/contact.php",

 "galeria" => "app/controllers/galeria.php",

 "post" => "app/controllers/single_post.php"

]

?>
