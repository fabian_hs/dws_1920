<?php
require_once 'utils/utils.php';
require_once 'utils/File.php';

require_once 'exceptions/FileException.php';
require_once 'exceptions/QueryException.php';
require_once 'exceptions/AppException.php';

require_once 'entity/ImagenGaleria.php';
require_once 'entity/Categoria.php';

require_once 'core/App.php';

require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';

require_once 'repository/ImagenGaleriaRepository.php';
require_once 'repository/CategoriaRepository.php';

$descripcion = "";
$mensaje = "";



  try {

    /*$config = require_once("app/config.php");
    //Guardamos la configuración en el contenedor
    App::bind("config", $config);*/
    //$connection = App::getConnection();

    //$queryBuilder = new QueryBuilder("imagenes","ImagenGaleria");
    $imagenGaleriaRepository = new ImagenGaleriaRepository();
    $categoriaRepository = new CategoriaRepository();

      if ($_SERVER["REQUEST_METHOD"]==="POST") {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        $categoria = trim(htmlspecialchars($_POST["categoria"]));

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
        $imagen = new File("imagen", $tiposAceptados);

        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY);
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY,ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

        $imagenGaleria = new ImagenGaleria($imagen->getFileName(),$descripcion, $categoria);
      //  $queryBuilder->save($imagenGaleria);
          $imagenGaleriaRepository->save($imagenGaleria);
        $mensaje = "se ha guardado la imagen en la BBDD.";
        }

        $arrayImagen = $imagenGaleriaRepository->findAll();
        $categorias = $categoriaRepository->findAll();


    } catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    }
    catch (QueryException $queryException) {

        $errores [] = $queryException->getMessage();

    }
    catch (AppException $appException) {

      throw new AppException("No se ha podido conectar con la BBDD");

    }




require __DIR__ . "/../view/galeria.view.php";
?>
