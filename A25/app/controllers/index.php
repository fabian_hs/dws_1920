<?php
require_once 'database/Connection.php';
require_once 'database/QueryBuilder.php';
require_once "database/IEntity.php";

require_once 'utils/utils.php';
require_once 'utils/File.php';


require_once 'entity/Categoria.php';
require_once "entity/ImagenGaleria.php";
require_once "entity/Asociado.php";

require_once 'exceptions/FileException.php';
require_once 'exceptions/QueryException.php';
require_once 'exceptions/AppException.php';

require_once 'core/App.php';

require_once 'repository/ImagenGaleriaRepository.php';
require_once 'repository/CategoriaRepository.php';



//$imagenes = array();

/*for ($i=1; $i <13 ; $i++) {
  $imagenes[$i] = new ImagenGaleria(($i).".jpg","descripcion imagen ".($i));
}*/
try {
/*  $config = require_once('app/config.php');
  App::bind("config",$config);*/
  $connection = App::getConnection();

  $imagenGaleriaRepository = new ImagenGaleriaRepository();
  $imagenes = $imagenGaleriaRepository->findAll();

} catch (QueryException $queryException) {
  $errores [] = $queryException->getMessage();
} catch (AppException $appException) {
  $errores [] = $appException->getMessage();
}



// Asociado
// Generar asociados
$asociado = [];
for($i = 1; $i <=4;$i++){
  $asociado[$i] = new Asociado($i,"log".$i,"Imagen de ".$i);
}

//echo count($asociado);

if (count($asociado)<=3){ // Comprueba que hay menos o igual a 3 asosiados.
  $asociado;
}else{ // En caso de haber mas, va al metodo y extrae tre asociados aleatoriamente.
  $asociado = tresAsociados($asociado);
}

require __DIR__ . "/../view/index.view.php";


?>
