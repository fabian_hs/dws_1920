<?php
include __DIR__ . "/partials/inicio-doc.part.php";
include __DIR__ . "/partials/nav.part.php";
?>
<!-- Principal Content Start -->
<div id="asociados">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>Asociados</h1>
            <hr>
            <?php
            if ($error != null){
              echo "<div class=\"alert alert-danger\">
               <strong>Error </strong>$error</div>";
            } ?>
            <div class="alert alert-dancer alert-dismissible" role="alert">
            <form class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"] ?>" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Nombre</label>
                        <input class="form-control" name="nombre" value="<?= $nombre ?>"></input>
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= $descripcion ?>"></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>
