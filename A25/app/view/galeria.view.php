<?php
include __DIR__ . "/partials/inicio-doc.part.php";
include __DIR__ . "/partials/nav.part.php";
?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <?php if(empty($errores)) : ?>
                <p><?= $mensaje ?></p>
                <?php else : ?>
                <ul>
                    <?php foreach($errores as $error) : ?>
                    <li><?= $error ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <form class="form-horizontal" action="<?=$_SERVER["REQUEST_URI"] ?>" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= $descripcion ?>"></textarea>
                        <label class="label-control">Categoría</label>
                        <select class="form-control" name="categoria">

                          <?php foreach ($categorias as $categoria) : ?>

                            <option value="<?= $categoria->getId()?>"><?= $categoria->getNombre();?></option>

                          <?php endforeach; ?>

                        </select>
                        <br>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>

                </div>
            </form>
        </div>
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Imagen</th>
                <th scope="col">Nombre</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Categoria</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($arrayImagen as $key ){ ?>
                <tr>
                  <td><?=$key->getId(); ?></td>
                  <td>
                    <img src="<?= ImagenGaleria::RUTA_IMAGENES_GALLERY.$key->getNombre(); ?>" alt="<?=$key->getDescripcion(); ?>" class="img-rounded center-block"></td>
                  <td><?=$key->getNombre(); ?></td>
                  <td><?=$key->getDescripcion(); ?></td>
                  <td><?=$imagenGaleriaRepository->getCategoria($key)->getNombre() ?></td>
                </tr>
              <?php  } ?>
            </tbody>
          </table>
        </div>
    </div>
</div>
<!-- Principal Content End -->
<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>
