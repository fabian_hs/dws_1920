<?php
/**
 *
 */

class Categoria implements IEntity
{
  private $id;
  private $nombre;
  private $numImagenes;


  function __construct($nombre = '',$numImagenes = '')
  {
    $this->nombre = $nombre;
    $this->numImagenes = $numImagenes;
    $this->id= null;

  }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Nombre
     *
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of Nombre
     *
     * @param mixed $nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of Num Imagenes
     *
     * @return mixed
     */
    public function getNumImagenes()
    {
        return $this->numImagenes;
    }

    /**
     * Set the value of Num Imagenes
     *
     * @param mixed $numImagenes
     *
     * @return self
     */
    public function setNumImagenes($numImagenes)
    {
        $this->numImagenes = $numImagenes;

        return $this;
    }

    public function toArray(): array{
      return[
        "id"=>$this-> getId(),
        "nombre"=>$this->getNombre(),
        "numImagenes"=>$this.getNumImagenes()
      ];
    }

}

 ?>
