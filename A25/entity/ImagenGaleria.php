<?php
require_once 'database/IEntity.php';
class ImagenGaleria implements IEntity
{

  private $id;
  private $nombre;
  private $descripcion;
  private $numVisualizciones;
  private $numLikes;
  private $numDownloads;
  private $categoria;


    public function __construct($nombre="",$descripcion="",$categoria=1,$numVisualizciones=0,$numLikes=0,$numDownloads=0){

      $this->id = null;
      $this->nombre=$nombre;
      $this->descripcion = $descripcion;
      $this->numVisualizaciones = $numVisualizciones;
      $this->numLikes = $numLikes;
      $this->numDownloads = $numDownloads;
      $this->categoria = $categoria;

    }

// Genera automaticamente sus getters y sus setters Ejercicio 1 /////////////////////////////////////

  // Getters & Setters

  /**
   * Get the value of ID
   *
   * @return mixed
   */
  public function getID()
  {
      return $this->id;
  }

  /**
   * Set the value of ID
   *
   * @param mixed $ID
   *
   * @return self
   */
  public function setID($ID)
  {
      $this->ID = $ID;

      return $this;
  }

  /**
  * Get the value of Nombre
  *
  * @return mixed
  */
  public function getNombre()
  {
    return $this->nombre;
  }

  /**
  * Set the value of Nombre
  *
  * @param mixed nombre
  *
  * @return self
  */
  public function setNombre($nombre)
  {
    $this->nombre = $nombre;
    return $this;
  }

  /**
  * Get the value of Descripcion
  *
  * @return mixed
  */
  public function getDescripcion()
  {
    return $this->descripcion;
  }

  /**
  * Set the value of Descripcion
  *
  * @param mixed descripcion
  *
  * @return self
  */
  public function setDescripcion($descripcion)
  {
    $this->descripcion = $descripcion;
    return $this;
  }

  /**
  * Get the value of Num Visualizaciones
  *
  * @return mixed
  */
  public function getNumVisualizaciones()
  {
    return $this->numVisualizaciones;
  }

  /**
  * Set the value of Num Visualizaciones
  *
  * @param mixed numVisualizaciones
  *
  * @return self
  */
  public function setNumVisualizaciones($numVisualizaciones)
  {
    $this->numVisualizaciones = $numVisualizaciones;
    return $this;
  }

  /**
  * Get the value of Num Likes
  *
  * @return mixed
  */
  public function getNumLikes()
  {
    return $this->numLikes;
  }

  /**
  * Set the value of Num Likes
  *
  * @param mixed numLikes
  *
  * @return self
  */
  public function setNumLikes($numLikes)
  {
    $this->numLikes = $numLikes;
    return $this;
  }

  /**
  * Get the value of Num Downloads
  *
  * @return mixed
  */
  public function getNumDownloads()
  {
    return $this->numDownloads;
  }

  /**
  * Set the value of Num Downloads
  *
  * @param mixed numDownloads
  *
  * @return self
  */
  public function setNumDownloads($numDownloads)
  {
    $this->numDownloads = $numDownloads;
    return $this;
  }

  /**
   * Get the value of Categoria
   *
   * @return mixed
   */
  public function getCategoria()
  {
      return $this->categoria;
  }

  /**
   * Set the value of Categoria
   *
   * @param mixed $categoria
   *
   * @return self
   */
  public function setCategoria($categoria)
  {
      $this->categoria = $categoria;

      return $this;
  }


  const RUTA_IMAGENES_PORTFOLIO ="images/index/portfolio/";

  public function getURLPortfolio() : string{

    return self::RUTA_IMAGENES_PORTFOLIO. $this->getNombre();

  }

  const RUTA_IMAGENES_GALLERY="images/index/gallery/";

  public function getURLPortGallery() : string{

    return self::RUTA_IMAGENES_GALLERY. $this->getNombre();

  }

  public function toArray(): array
  {

 return [

   "id"=>$this->getId(),

   "nombre"=>$this->getNombre(),

   "descripcion"=>$this->getDescripcion(),

   "numVisualizaciones"=>$this->getNumVisualizaciones(),

   "numLikes"=>$this->getNumLikes(),

   "numDownloads"=>$this->getNumDownloads(),

   "categoria"=>$this->getCategoria()
 ];

 }


}
?>
