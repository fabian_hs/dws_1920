
<!-- Navigation Bar -->
   <nav class="navbar navbar-fixed-top navbar-default">
     <div class="container">
         <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a  class="navbar-brand page-scroll" href="#page-top">
              <span>[PHOTO]</span>
            </a>
         </div>
         <div class="collapse navbar-collapse navbar-right" id="menu">
            <ul class="nav navbar-nav">
              <li class="<?php Active('index.php','/A17/');?> lien"><a href="<?php Url('index.php','/A17/');?>"><i class="fa fa-home sr-icons"></i> Home</a></li>
              <li class="<?php Active('about.php');?> lien"><a href="<?php Url('about.php');?>"><i class="fa fa-bookmark sr-icons"></i> About</a></li>
              <li class="<?php Active('blog.php');?> lien"><a href="<?php Url('blog.php');?>"><i class="fa fa-file-text sr-icons"></i> Blog</a></li>
              <li class="<?php Active('contact.php');?> lien"><a href="<?php Url('contact.php');?>"><i class="fa fa-phone-square sr-icons"></i> Contact</a></li>
              <li class="<?php Active('galeria.php');?> lien"><a href="<?php Url('galeria.php');?>"><i class="fa fa-image sr-icons"></i> Galeria</a></li>
              <li class="<?php Active('asociados.php');?>"><a href="<?php Url('asociados.php');?>"><i class="fa fa-hand-o-right"></i>Asociados</a></li>
            </ul>
         </div>
     </div>
   </nav>
<!-- End of Navigation Bar -->
