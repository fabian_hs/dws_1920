<?php
require 'utils/utils.php';
require 'exceptions/FileException.php';
require 'utils/File.php';


$descripcion = "";
$mensaje = "";


if ($_SERVER["REQUEST_METHOD"]==="POST") {

    try {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
        $imagen = new File("imagen", $tiposAceptados);
        $mensaje = "Datos enviados";

    } catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    }
  }

require "view/galeria.view.php";
?>
