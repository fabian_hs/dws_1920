<?php
/**
 *
 */
class File
{

    private $file;
    private $fileName;

    public function __construct(string $fileName, array $arrTypes) {

        $this->file = $_FILES[$fileName];

        $this->fileName = "";

        if (($this->file["name"] =="")) {

            throw new FileException("Debes especificar un fichero", 1);

        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {

            switch ($this-file["error"]) {

                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:


                    throw new FileException("Error de tamaño", 1);

                case UPLOAD_ERR_PARTIAL:

                    throw new FileException("Error de archivo incompleto",1);

                default:

                    throw new FileException("error genérico en la súbida del fichero",1);

                    break;

            }

        }

        if (in_array($this->file["type"], $arrTypes)===false) {

            throw new FileException("error de tipo",1);

        }

    }

    /**
     * Get the value of File Name
     *
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

}


 ?>
